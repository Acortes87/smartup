-- MOVIES
INSERT INTO `MOVIES` (`title`, `genre`, `year`) VALUES ('titulo1_TEST', 'terror', '1995');
INSERT INTO `MOVIES` (`title`, `genre`, `year`) VALUES ('titulo2_TEST', 'comedia', '2000');
INSERT INTO `MOVIES` (`title`, `genre`, `year`) VALUES ('titulo3_TEST', 'accion', '2012');

-- REPARTO MOVIES
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR1_TEST', 'APLL1', 'APLL1', 25, 'Protagonista', 1);
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR2_TEST', 'APLL2', 'APLL2', 23, 'Secundario', 1);
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR3_TEST', 'APLL1', 'APLL1', 25, 'Protagonista', 2);
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR4_TEST', 'APLL2', 'APLL2', 23, 'Secundario', 2);