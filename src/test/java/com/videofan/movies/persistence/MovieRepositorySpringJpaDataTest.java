package com.videofan.movies.persistence;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.videofan.exception.ResourceNotFounException;
import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.persistence.data.RepositoryMovie;
import com.videofan.movies.persistence.model.Actor;
import com.videofan.movies.persistence.model.Movie;

@ExtendWith(SpringExtension.class)
class MovieRepositorySpringJpaDataTest {

	@Mock
	private RepositoryMovie repoMovie;

	@InjectMocks
	private MovieRepositorySpringJpaData movieRepo;

	@Test
	void testFoundDetailMovieFromId() throws ResourceNotFounException {

		when(repoMovie.findById(Mockito.anyLong())).thenReturn(Optional.of(UtilData.getMovieMock()));

		Optional<MovieDto> sda = movieRepo.findOneMovieById(1);

		assertTrue(sda.isPresent());


	}
	
	@Test
	void testNotFoundDetailMovieFromIdReturnOptionalEmpty() {

		when(repoMovie.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		
		Optional<MovieDto> result = movieRepo.findOneMovieById(1);
		
		assertEquals(Optional.empty(), result);
	}
	
	@Test
	void testFindAllMoviesNotFound() {
		
		when(repoMovie.findAll()).thenReturn(Collections.emptyList());
		
		List<MovieDto> moviesAll = movieRepo.findAllMovies();
		
		assertNotNull(moviesAll);
		
	}

	@Test
	void testFindAllMoviesFoundVariousMoviesWhitOutCastOfActors() {
		
		when(repoMovie.findAll()).thenReturn(Arrays.asList(UtilData.getMovieMock(), UtilData.getMovieMock()));
		
		List<MovieDto> moviesAll = movieRepo.findAllMovies();
		
		assertNotNull(moviesAll);
		
	}
	
	@Test
	void testFindAllMoviesFoundVariousMoviesWhitCastOfActors() {
		
		Movie movieMock = UtilData.getMovieMock();
		Actor actor = UtilData.getActorMock();
		
		movieMock.setCastOfActors(Arrays.asList(actor,actor));
		
		when(repoMovie.findAll()).thenReturn(Arrays.asList(movieMock,movieMock));
		
		List<MovieDto> moviesAll = movieRepo.findAllMovies();
		
		assertNotNull(moviesAll);
		
	}
	
	
	

	@Test
	void testCreateMovie() {
		
		MovieDto.from(UtilData.getMovieDtoMock())
		.movieReparto(Arrays.asList(UtilData.getActorDtoMock(),UtilData.getActorDtoMock()))
		.build();
		
		when(repoMovie.save(Mockito.any(Movie.class))).then(p -> {

			Movie as = p.getArgument(0);
			as.setId(1);
			return as;
		});
		
		MovieDto movieCreated = movieRepo.createMovie(UtilData.getMovieDtoMock());
		
		assertNotNull(movieCreated);
		assertEquals(1, movieCreated.getId());
	}

}
