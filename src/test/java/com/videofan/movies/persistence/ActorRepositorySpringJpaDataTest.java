package com.videofan.movies.persistence;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.videofan.exception.ResourceNotFounException;
import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.persistence.data.RepositoryActor;

@ExtendWith(SpringExtension.class)
public class ActorRepositorySpringJpaDataTest {

	@Mock
	private RepositoryActor repoActor;
	
	@Mock
	private MovieRepository movieRepository;

	@InjectMocks
	private ActorRepositorySpringJpaData actorRepo;
	
	@Test
	public void AddCastOfActorFromMovies() {
		
		List<ActorDto> actores = Arrays.asList(UtilData.getActorDtoMock(), UtilData.getActorDtoMock());
		
		when(movieRepository.findOneMovieById(Mockito.anyLong())).thenReturn(Optional.of(UtilData.getMovieDtoMock()));
		
		when(repoActor.saveAll(Mockito.any())).thenReturn(Arrays.asList(UtilData.getActorMock()));
		
		List<ActorDto> asd = actorRepo.createCastOfActors(1, actores);
		
		assertNotNull(asd);
		
		
	}
	
	@Test
	public void AddCastOfActorFromMoviesNotFoundMovie() {
		
		List<ActorDto> actores = Arrays.asList(UtilData.getActorDtoMock(), UtilData.getActorDtoMock());
		
		when(movieRepository.findOneMovieById(Mockito.anyLong())).thenReturn(Optional.empty());
		
		assertThrows(ResourceNotFounException.class, ()-> actorRepo.createCastOfActors(1, actores));
		
	}

}
