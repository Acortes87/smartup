package com.videofan.movies.persistence;

import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.persistence.model.Actor;
import com.videofan.movies.persistence.model.Movie;

public class UtilData {
	
	public static Movie getMovieMock () {
		
		Movie movieMock;
		
		movieMock = new Movie();
		movieMock.setId(1l);
		movieMock.setTitle("title");
		movieMock.setGenre("gene");
		
		return movieMock;
		
	}
	
	public static Actor getActorMock() {
		Actor actor = new Actor();
		actor.setApellido1("app");
		actor.setNombre("name");
		actor.setId(1l);
		return actor;
	}
	
	public static MovieDto getMovieDtoMock() {
		
		return new MovieDto.Builder()
				.movieTitle("tituloMovie")
				.movieGenre("generoMovie")
				.movieYear(2002)
				.build();
	}
	
	public static ActorDto getActorDtoMock() {
		return new ActorDto.BuilderActor()
				.actorName("Name1Test")
				.actorApellido1("Apll1Test")
				.actorApellido2("Apll2Test")
				.actorCategoriaReparto("Secundario")
				.actorEdad(28)
				.build();
	}

}
