package com.videofan.movies.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.persistence.UtilData;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class MoviesControllerTestIT {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
    ObjectMapper objectmapper;

	@Test
	void testGetAllMovies() throws Exception {
		MvcResult mvcResult = mockMvc.perform(
				get("/movies"))
				.andDo(print()).andExpect(status().isOk()).andReturn();
		 
	 assertThat(mvcResult.getResponse().getContentAsString()).isNotBlank();
	}

	@Test
	void testGetDetalleMovieOk() throws Exception {
		
		MvcResult mvcResult = mockMvc.perform(
				get("/movies/1"))
				.andDo(print()).andExpect(status().isOk()).andReturn();
		 
	 assertThat(mvcResult.getResponse().getContentAsString()).isNotBlank();
	}
	
	@Test
	void testGetDetalleMovieNotFoundResource() throws Exception {
		
		MvcResult mvcResult = mockMvc.perform(
				get("/movies/10")).andExpect(status().isNotFound()).andReturn();
		 
	 assertThat(mvcResult.getResponse().getContentAsString()).isBlank();
	}
	
	@Test
	void testCreateMovie() throws Exception {
		
		MovieDto movie = MovieDto.from(UtilData.getMovieDtoMock())
				.movieReparto(Arrays.asList(UtilData.getActorDtoMock(),UtilData.getActorDtoMock()))
				.build();
		
//		MovieDto movie = UtilData.getMovieDtoMock();
		
		
		String response = mockMvc.perform(post("/movies")
                .content(objectmapper.writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andReturn().getResponse().getContentAsString();
		
		assertThat(response).isEmpty();
	}
	
	
	@Test
	void testAddcastOfActorsToMovie() throws Exception {
		
		List<ActorDto> listCastOfActor = Arrays.asList(UtilData.getActorDtoMock(),UtilData.getActorDtoMock());

		String response = mockMvc.perform(post("/movies/1/castOfActors")
                .content(objectmapper.writeValueAsString(listCastOfActor))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andReturn().getResponse().getContentAsString();
		
		assertThat(response).isEmpty();
	}
	
	@Test
	void testFailAddcastOfActorsToMovieByMovieNotFound() throws Exception {
		
		List<ActorDto> listCastOfActor = Arrays.asList(UtilData.getActorDtoMock(),UtilData.getActorDtoMock());

		String response = mockMvc.perform(post("/movies/5/castOfActors")
                .content(objectmapper.writeValueAsString(listCastOfActor))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andReturn().getResponse().getContentAsString();
		
		assertThat(response).isNotBlank();
	}
	

}
