package com.videofan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAsync
@EnableJpaAuditing
@EnableSwagger2
public class VideoFanApplication {
	
	@Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("SmartUp_VideoFanAPI")
        		.apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Spring API REST SmartUp")
				.description("REST API ejemplo para proyecto SmartUp")
				.version("0.1")
				.contact(new Contact("Angel Cortés", "http://ange-cortes.com", "angel.cortesmaqueda@soprasteria.com"))
				.build();
	}


	public static void main(String[] args) {
		SpringApplication.run(VideoFanApplication.class, args);
	}

}
