package com.videofan.movies.service;

import java.util.List;

import com.videofan.movies.dto.ActorDto;

public interface ActorService {

	List<ActorDto> createCastOfActors(long id, List<ActorDto> castOfActors);

}
