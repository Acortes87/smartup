package com.videofan.movies.service;

import java.util.List;
import java.util.Optional;

import com.videofan.movies.dto.MovieDto;

public interface MoviesService {
	
	Optional<MovieDto> getDetailMovie(long id);
	List<MovieDto> getAllMovies();
	MovieDto createMovie (MovieDto movie);

}
