package com.videofan.movies.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.persistence.MovieRepository;

@Service
public class MoviesServiceImpl implements MoviesService {
	
	private MovieRepository movieRepository;

	public MoviesServiceImpl(MovieRepository movieRepository) {
		super();
		this.movieRepository = movieRepository;
	}

	@Override
	public Optional<MovieDto> getDetailMovie(long id){

		return movieRepository.findOneMovieById(id);
	}

	@Override
	public List<MovieDto> getAllMovies() {

		return movieRepository.findAllMovies();
	}

	@Override
	public MovieDto createMovie(MovieDto movie) {
		
		return movieRepository.createMovie(movie);
	}
	
	

}
