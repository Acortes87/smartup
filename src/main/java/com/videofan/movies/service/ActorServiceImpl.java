package com.videofan.movies.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.persistence.ActorRepository;

@Service
public class ActorServiceImpl implements ActorService {
	
	private ActorRepository actorRepository;

	public ActorServiceImpl(ActorRepository actorRepository) {
		super();
		this.actorRepository = actorRepository;
	}

	@Override
	public List<ActorDto> createCastOfActors(long id, List<ActorDto> castOfActors) {
	
		return  actorRepository.createCastOfActors(id, castOfActors);
	}

}
