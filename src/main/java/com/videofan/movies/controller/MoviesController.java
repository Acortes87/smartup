package com.videofan.movies.controller;

import java.net.URI;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.videofan.exception.ResourceNotFounException;
import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.service.ActorService;
import com.videofan.movies.service.MoviesService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/movies")
public class MoviesController {

	private MoviesService movieService;
	private ActorService actorService;

	public MoviesController(MoviesService movieService, ActorService actorService) {
		super();
		this.movieService = movieService;
		this.actorService=actorService;
	}

	@GetMapping
	@ApiOperation(value = "Obten una lista de peliculas con detalles basicos")
	public ResponseEntity<List<MovieDto>> getAllMovies(Pageable pageable) {
		
		List<MovieDto> resultListMovies = movieService.getAllMovies();

		if (!CollectionUtils.isEmpty(resultListMovies)) {
			return ResponseEntity.ok(resultListMovies);
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}

	}

	@GetMapping("/{movie-id}")
	@ApiOperation(value = "Obten una pelicula con todos los atributos")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Obtiene los detalles de una pelicula"),
            @ApiResponse(code = 404, message = "Pelicula no registrada")
    })
	public ResponseEntity<MovieDto> getDetalleMovie(@PathVariable(name = "movie-id") long id)
			throws ResourceNotFounException {

		return movieService.getDetailMovie(id)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	@ApiOperation(value = "Crea una pelicula")
	public ResponseEntity<MovieDto> createMovie(@RequestBody MovieDto movie) {

		MovieDto movieCreated = movieService.createMovie(movie);

		// Create resource location
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(movieCreated.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	@PostMapping("/{movie-id}/castOfActors")
	@ApiOperation(value = "Agrega el reparto a una pelicula")
	public ResponseEntity<?> addActorsToMovie(@PathVariable("movie-id") long id,
			@RequestBody List<ActorDto> castOfActors) {
		
		actorService.createCastOfActors(id, castOfActors);

		// Create resource location
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(id)
				.toUri();

		return ResponseEntity.created(location).build();
	}
	
}
