package com.videofan.movies.controller.exceptionHandler;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.videofan.exception.ResourceNotFounException;

@ControllerAdvice
public class CustomExceptionHandler {
	
	@ExceptionHandler(ResourceNotFounException.class)
	public final ResponseEntity<ErrorResponse> handleHeaderException(ResourceNotFounException ex,
			HttpServletRequest request) {
		
		ErrorResponse error = new ErrorResponse.Builder()
				.setTitle("Recurso no Encontrado")
				.setDetail(ex.toString())
				.build();

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	}


	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, HttpServletRequest request) {

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();

		ErrorResponse error = new ErrorResponse.Builder()
				.setSource(location.toString())
				.setTitle(ex.getMessage())
				.setDetail(ex.toString())
				.build();

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
	}
}