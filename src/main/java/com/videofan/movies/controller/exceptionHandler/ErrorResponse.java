package com.videofan.movies.controller.exceptionHandler;

public class ErrorResponse {

	private String source;
	private String title;
	private String detail;

	private ErrorResponse() {

	}

	public String getSource() {
		return source;
	}

	public String getTitle() {
		return title;
	}

	public String getDetail() {
		return detail;
	}

	public static class Builder {

		private ErrorResponse errorResponse;

		public Builder() {
			this.errorResponse = new ErrorResponse();
		}

		public Builder setSource(String source) {
			errorResponse.source = source;
			return this;
		}

		public Builder setTitle(String title) {
			errorResponse.title = title;
			return this;
		}

		public Builder setDetail(String detail) {
			errorResponse.detail = detail;
			return this;
		}

		public ErrorResponse build() {
			return errorResponse;
		}

	}

}
