package com.videofan.movies.persistence.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.videofan.movies.persistence.model.Actor;

public interface RepositoryActor extends JpaRepository<Actor, Long>  {

}
