package com.videofan.movies.persistence.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.videofan.movies.persistence.model.Movie;

public interface RepositoryMovie extends JpaRepository<Movie, Long> {

}
