package com.videofan.movies.persistence;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.videofan.exception.ResourceNotFounException;
import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.persistence.data.RepositoryActor;
import com.videofan.movies.persistence.mapper.Mapper;
import com.videofan.movies.persistence.model.Actor;
import com.videofan.movies.persistence.model.Movie;

@Repository
public class ActorRepositorySpringJpaData implements ActorRepository {
	
	private RepositoryActor repositoryActor;
	private MovieRepository movieRepository;
	
	public ActorRepositorySpringJpaData(RepositoryActor repositoryActor, MovieRepository movieRepository) {
		super();
		this.repositoryActor = repositoryActor;
		this.movieRepository =movieRepository;
	}



	@Override
	public List<ActorDto> createCastOfActors(long id, List<ActorDto> castOfActors) {
		
		movieRepository.findOneMovieById(id)
			.orElseThrow(()-> new ResourceNotFounException(Movie.class +": "+ id));
		
		List<Actor> entitysWithMovieRefToSave = Mapper.mapperList(castOfActors, Mapper::actorDtoToEntity).stream()
		.map(actor -> this.addMovieRefToActor(id, actor))
		.collect(Collectors.toList());
		
		return repositoryActor.saveAll(entitysWithMovieRefToSave).stream()
				.map(Mapper::actorEntityToDto)
				.collect(Collectors.toList());

	}
	
	private Actor addMovieRefToActor (long idMovie, Actor actor) {
		
		Movie movie = new Movie();
		movie.setId(idMovie);
		actor.setMovie(movie);
		return actor;
		
	}

}
