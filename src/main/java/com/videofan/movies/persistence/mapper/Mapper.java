package com.videofan.movies.persistence.mapper;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.videofan.movies.dto.ActorDto;
import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.persistence.model.Actor;
import com.videofan.movies.persistence.model.Movie;

public class Mapper {

	public static MovieDto movieEntityToDto(Movie movie) {

		return new MovieDto.Builder()
				.movieTitle(movie.getTitle())
				.movieId(movie.getId())
				.movieYear(movie.getYear())
				.movieReparto(Mapper.mapperList(movie.getCastOfActors(), Mapper::actorEntityToDto))
				.build();
	}
	
	public static MovieDto movieEntityBasicToDto(Movie movie) {

		return new MovieDto.Builder()
				.movieTitle(movie.getTitle())
				.movieId(movie.getId())
				.movieYear(movie.getYear())
				.build();
	}

	public static ActorDto actorEntityToDto(Actor actor) {

		return new ActorDto.BuilderActor()
				.actorId(actor.getId())
				.actorName(actor.getNombre())
				.actorApellido1(actor.getApellido1())
				.actorApellido2(actor.getApellido2())
				.actorEdad(actor.getEdad())
				.actorCategoriaReparto(actor.getCategoriaReparto())
				.build();

	}
	
	public static Movie movieDtoToEntity(MovieDto movieDto) {
		
		Movie movie = new Movie();
		movie.setId(movieDto.getId());
		movie.setTitle(movieDto.getTitle());
		movie.setGenre(movieDto.getTitle());
		movie.setYear(movieDto.getYear());
		
		movie.setCastOfActors(
				Mapper.mapperList(movieDto.getCastOfActors(), Mapper::actorDtoToEntity));
		
		return movie;
	}
	
	public static Actor actorDtoToEntity(ActorDto actorDto) {
		
		Actor actor = new Actor();
		actor.setNombre(actorDto.getNombre());
		actor.setApellido1(actorDto.getNombre());
		actor.setEdad(actorDto.getEdad());
		actor.setCategoriaReparto(actorDto.getCategoriaReparto());
		actor.setId(actorDto.getId());
				
		return actor;
	}

	public static <R, T> List<R> mapperList(List<T> castOfActors,Function<T, R> convers) {
		
		return Optional.ofNullable(castOfActors)
				.map(lstCastOfActor -> castOfActors.stream().map(convers)
						.collect(Collectors.toList()))
						.orElse(Collections.emptyList());

	}

}
