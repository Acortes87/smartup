package com.videofan.movies.persistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.videofan.movies.dto.MovieDto;
import com.videofan.movies.persistence.data.RepositoryMovie;
import com.videofan.movies.persistence.mapper.Mapper;
import com.videofan.movies.persistence.model.Movie;

@Repository
@Transactional
public class MovieRepositorySpringJpaData implements MovieRepository{
	
	private RepositoryMovie repoMovie;
	
	public MovieRepositorySpringJpaData(RepositoryMovie repoMovie) {
		super();
		this.repoMovie = repoMovie;
	}

	@Override
	public Optional<MovieDto> findOneMovieById(long id)  {

		return repoMovie.findById(id)
				.map(Mapper::movieEntityToDto);
	}

	@Override
	public List<MovieDto> findAllMovies() {

		return repoMovie.findAll().stream()
				.map(Mapper::movieEntityBasicToDto)
				.collect(Collectors.toList());

	}

	@Override
	public MovieDto createMovie(MovieDto movie) {
		
		Movie movieEntity =Mapper.movieDtoToEntity(movie);
		
		Movie movieCreated =repoMovie.save(movieEntity);
		
		return Mapper.movieEntityToDto(movieCreated);

	}

}
