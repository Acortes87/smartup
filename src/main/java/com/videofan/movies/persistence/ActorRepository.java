package com.videofan.movies.persistence;

import java.util.List;

import com.videofan.movies.dto.ActorDto;

public interface ActorRepository {

	List<ActorDto> createCastOfActors(long id, List<ActorDto> castOfActors);

}
