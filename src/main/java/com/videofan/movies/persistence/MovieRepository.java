package com.videofan.movies.persistence;

import java.util.List;
import java.util.Optional;

import com.videofan.movies.dto.MovieDto;

public interface MovieRepository {
	
	Optional<MovieDto> findOneMovieById(long id);
	List<MovieDto> findAllMovies ();
	MovieDto createMovie (MovieDto movie);

}
