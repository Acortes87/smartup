package com.videofan.movies.dto;

public class ActorDto {

	private long id;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private int edad;
	private String categoriaReparto;

	private ActorDto() {
	}

	public long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public int getEdad() {
		return edad;
	}

	public String getCategoriaReparto() {
		return categoriaReparto;
	}

	public static class BuilderActor {

		private ActorDto actor;

		public BuilderActor() {
			actor = new ActorDto();
		}

		public BuilderActor actorId(long id) {
			actor.id = id;
			return this;
		}

		public BuilderActor actorName(String name) {
			actor.nombre = name;
			return this;
		}

		public BuilderActor actorApellido1(String apellido1) {
			actor.apellido1 = apellido1;
			return this;
		}

		public BuilderActor actorApellido2(String apellido2) {
			actor.apellido2 = apellido2;
			return this;
		}

		public BuilderActor actorEdad(int edad) {
			actor.edad = edad;
			return this;
		}

		public BuilderActor actorCategoriaReparto(String categoria) {
			actor.categoriaReparto = categoria;
			return this;
		}

		public ActorDto build() {
			return actor;
		}

	}

}
