package com.videofan.movies.dto;

import java.util.ArrayList;
import java.util.List;

public class MovieDto {

	private long id;
	private String title;
	private String genre;
	private int year;
	private List<ActorDto> castOfActors;
	
	private MovieDto () {
		castOfActors = new ArrayList<>();
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getGenre() {
		return genre;
	}

	public int getYear() {
		return year;
	}

	public List<ActorDto> getCastOfActors() {
		return castOfActors;
	}

	public static class Builder {

		private MovieDto movie;

		public Builder() {
			movie = new MovieDto();
		}
		private Builder (MovieDto v) {
			this.movie = new MovieDto();
			movie.id = v.getId();
			movie.title = v.getTitle();
			movie.genre = v.getGenre();
			movie.year = v.getYear();
			movie.castOfActors= new ArrayList<>(v.getCastOfActors());
		}

		public Builder movieId(long id) {
			movie.id = id;
			return this;
		}

		public Builder movieTitle(String title) {
			movie.title = title;
			return this;
		}

		public Builder movieGenre(String genre) {
			movie.genre = genre;
			return this;
		}

		public Builder movieYear(int year) {
			movie.year = year;
			return this;
		}

		public Builder movieReparto(List<ActorDto> castOfActors) {
			movie.castOfActors = castOfActors;
			return this;
		}

		public MovieDto build() {
			return movie;
		}

	}
	
	public static Builder from (MovieDto movie) {
		
		return new Builder(movie);
	}

}
