package com.videofan.aop.auditoria;

import java.time.Duration;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;



import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
class AuditServiceSpringJpa implements AuditService {
	
	private ObjectMapper mapper;
	private AuditRepository auditRepo;

	public AuditServiceSpringJpa(ObjectMapper mapper, AuditRepository auditRepo) {
		super();
		this.auditRepo = auditRepo;
		this.mapper=mapper;
	}
		

	@Async
	@Override
	public void save(ProceedingJoinPoint joinPoint, ResponseEntity<?> resultado, Duration duration) throws JsonProcessingException {
		String dataInput = obtainJsonForInputParams(joinPoint);

		String serviceName =obtainServiceName(joinPoint);
		
		String dataOutput = mapper.writeValueAsString(resultado.getBody());
		
		AuditRestApiEntity auditEntity = new AuditRestApiEntity();
		auditEntity.setServiceName(serviceName);
		auditEntity.setInputData(dataInput);
		auditEntity.setOutputData(dataOutput);
		auditEntity.setTimeDuration(duration.getSeconds());
		
		auditRepo.saveAndFlush(auditEntity);
	}
	
	private String obtainServiceName(ProceedingJoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();
		return signature.toShortString();
	}

	private String obtainJsonForInputParams(ProceedingJoinPoint joinPoint) {
		Object[] valueParms = joinPoint.getArgs();
		CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
		String[] nameParms = codeSignature.getParameterNames();

		Map<String, Object> inputMap = IntStream.range(0, nameParms.length).boxed()
				.collect(Collectors.toMap(i -> nameParms[i], i -> valueParms[i]));

		try {
			return mapper.writeValueAsString(inputMap);
		} catch (Exception e) {
			return "{\"error\":\"Error en el parseo de los datos de entrada: "+e.getMessage()+" \"}";
		}
	}

}
