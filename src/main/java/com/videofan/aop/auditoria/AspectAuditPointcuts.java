package com.videofan.aop.auditoria;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
class AspectAuditPointcuts {

	@Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
	public void restControllerClassMethods() {
		// define un ambito de actuacion para todos metodos de una clase anotada con RestController
	}

}
