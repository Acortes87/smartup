package com.videofan.aop.auditoria;

import java.time.Duration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

interface AuditService {

	void save(ProceedingJoinPoint joinPoint, ResponseEntity<?> resultado, Duration duration) throws JsonProcessingException;

}
