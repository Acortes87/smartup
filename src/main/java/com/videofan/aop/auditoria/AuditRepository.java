package com.videofan.aop.auditoria;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditRepository extends JpaRepository<AuditRestApiEntity, Long>{

}
