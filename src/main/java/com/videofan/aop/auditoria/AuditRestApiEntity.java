package com.videofan.aop.auditoria;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
class AuditRestApiEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_AUDIT")
	private long id;
	private String serviceName;
	@Column(length = 1000)
	private String inputData;
	@Column(length = 1000)
	private String outputData;
	private long timeDuration;
	
	@Column(name = "CREATED_ON")
	@CreatedDate
	@Convert(converter = LocalDateAttributeConverter.class)
    private LocalDateTime createdOn;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getInputData() {
		return inputData;
	}
	public void setInputData(String inputData) {
		this.inputData = inputData;
	}
	public String getOutputData() {
		return outputData;
	}
	public void setOutputData(String outputData) {
		this.outputData = outputData;
	}
	public long getTimeDuration() {
		return timeDuration;
	}
	public void setTimeDuration(long timeDuration) {
		this.timeDuration = timeDuration;
	}

}
