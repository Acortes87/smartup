package com.videofan.aop.auditoria;

import java.time.Duration;
import java.time.Instant;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AspectAudit {

	
	AuditService auditService;

	public AspectAudit(AuditService auditService) {
		
		this.auditService=auditService;
	}

	@Around("AspectAuditPointcuts.restControllerClassMethods()")
	public Object AuditController(ProceedingJoinPoint joinPoint) throws Throwable {

		Instant instan1 = Instant.now();

		Object resultado =  joinPoint.proceed();

		Instant instan2 = Instant.now();

		auditService.save(joinPoint, (ResponseEntity<?>) resultado, Duration.between(instan1, instan2));

		return resultado;
	}

	

}
