-- MOVIES
INSERT INTO `MOVIES` (`title`, `genre`, `year`) VALUES ('titulo1', 'terror', '1995');
INSERT INTO `MOVIES` (`title`, `genre`, `year`) VALUES ('titulo2', 'comedia', '2000');
INSERT INTO `MOVIES` (`title`, `genre`, `year`) VALUES ('titulo3', 'accion', '2012');

-- REPARTO MOVIES
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR1', 'APLL1', 'APLL1', 25, 'Protagonista', 1);
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR2', 'APLL2', 'APLL2', 23, 'Secundario', 1);
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR3', 'APLL1', 'APLL1', 25, 'Protagonista', 2);
INSERT INTO `REPARTO` (`nombre`, `apellido1`, `apellido2`, `edad`, `categoria_Reparto`, `id_movie`) VALUES ('ACTOR4', 'APLL2', 'APLL2', 23, 'Secundario', 2);