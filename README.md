
# Proyecto de ejemplo del grupo SmartUp

Consiste en una aplicacion web de películas. Cada película podrá contener el reparto de actores.

# El proyecto se divide por paquetería en:


com.videofan.movies.controller --> Entrada/Salida a traves de HTTP REST

com.videofan.movies.controller.exceptionHandler --> Modelado de las respuestas de excepciones en formato REST

com.videofan.movies.service --> Capa de negocio

com.videofan.movies.dto --> Clases/POJOS que definen el modelo de negocio

com.videofan.movies.persistence.*  --> capa encargada de la persistencia en BBDD

com.videofan.aop.auditoria --> Filtros que se encargan de auditar en BBDD cada llamada a la aplicacion Web

Tambien existen test unitarios y de Integración.


# URL


Swagger
****************
http://localhost:8888/api/v2/api-docs
http://localhost:8888/api/swagger-ui.html#/
